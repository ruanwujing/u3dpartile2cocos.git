# u3dpartile2cocos

#### 介绍
cocoscreator的一个插件，把unity的粒子特效转换成cocos

#### 使用说明

1.  下载，将u3dparticle2cocos文件夹放入ccc项目的extension中
2.  将u3dparticle2cocos.ts脚本放入项目中
3.  将unity中的粒子特效预制体保存格式设置为forceText:Editor->ProjectSettings->Editor->Mode 设置为ForceText
4.  保存预制体后，将所有需要转换的预制体放到一个新建的文件夹中
5.  打开项目，开启u3dparticle2cocos插件，在上方工具栏点开插件面板，面板里选中刚才新建的文件夹，点击按钮生成cocos节点

#### 相关说明
1.  插件会在当前选中节点下生成一个root节点，root节点上自动添加u3dParticle2cocos组件，借用该组件解析u3d预制体信息，生成cocos节点到root下。
2.  材质需要自己重新拖拽绑定，部分属性，u3d和cocos之间有差异，请根据具体情况细调
3.  插件开发时，使用的u3d版本是2020.1.0f1,cocoscreator版本为3.7。目前还没有试过其他版本的情况。
4.  由于解析，赋值逻辑是放在组件里写的，有能力的朋友完全可以自己看着改，不需要会开发插件。
