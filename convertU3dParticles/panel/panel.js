'use strict';
const fs = require('fs');
const path = require('path');

// html text
exports.template = fs.readFileSync(path.join(__dirname, 'panel.html'), 'utf8');
// style text
exports.style = `
ui-button { margin: 5px 5%; }
`;
// html selector after rendering
exports.$ = {    
    dir: 'ui-file',
    btn: 'ui-button',
};
// method on the panel
exports.methods = {};
// event triggered on the panel
exports.listeners = {};

let change = '';

// Triggered when the panel is rendered successfully
exports.ready = async function () {
    this.$.btn.addEventListener('confirm', ()=>{
        console.log("confirm", this.$.dir)
        const dir = this.$.dir.value;
        if (!dir) {
            console.warn("请选择一个文件夹")
            return
        }
        Editor.Message.send("u3dparticle-cocos", "convert", dir)
    });

};

// Triggered when the panel is actually closed
exports.close = async function () { };
