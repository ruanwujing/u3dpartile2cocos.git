'use strict';

const cache = {};
var YAML = require("yamljs")
const fs = require('fs');

let teststr = `%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1 &4129666436681368129
GameObject:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  serializedVersion: 6
  m_Component:
  - component: {fileID: 891580409162488113}
  m_Layer: 0
  m_Name: GameObject
  m_TagString: Untagged
  m_Icon: {fileID: 0}
  m_NavMeshLayer: 0
  m_StaticEditorFlags: 0
  m_IsActive: 1
--- !u!4 &891580409162488113
Transform:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 4129666436681368129}
  m_LocalRotation: {x: 0, y: 0, z: 0, w: 1}
  m_LocalPosition: {x: 0, y: 0, z: 0}
  m_LocalScale: {x: 1, y: 1, z: 1}
  m_Children: []
  m_Father: {fileID: 5483412303715139397}
  m_RootOrder: 0
  m_LocalEulerAnglesHint: {x: 0, y: 0, z: 0}
--- !u!1 &5389854303961652744
GameObject:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  serializedVersion: 6
  m_Component:
  - component: {fileID: 5483412303715139397}
  m_Layer: 0
  m_Name: GameObject
  m_TagString: Untagged
  m_Icon: {fileID: 0}
  m_NavMeshLayer: 0
  m_StaticEditorFlags: 0
  m_IsActive: 1
--- !u!4 &5483412303715139397
Transform:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 5389854303961652744}
  m_LocalRotation: {x: 0, y: 0, z: 0, w: 1}
  m_LocalPosition: {x: 4.2634153, y: 9.106887, z: 6.4120226}
  m_LocalScale: {x: 1, y: 1, z: 1}
  m_Children:
  - {fileID: 891580409162488113}
  m_Father: {fileID: 0}
  m_RootOrder: 0
  m_LocalEulerAnglesHint: {x: 0, y: 0, z: 0}

`



var parseU3dPrefab = function(yamlStr) {
    // 删除无用tag
    let content = yamlStr.replace(/%TAG !u!.*?\n/g, "")

    // 拆分对象
    let sp = content.split(/--- !u!.*?\n/g)
    // 将对象id放入到对象中作为属性fileID
    // 将对象类型放到对象中作为objectType
}
/**
 * 插件定义的方法
 * Methods defined by plug-ins
 * 可以在 package.json 里的 contributions 里定义 messages 触发这里的方法
 * And of course, messages can be defined in the contributions section in package.JSON to trigger the method here
 */
exports.methods = {
    async convert(path) {
        let filesList = fs.readdirSync(path)
        let root_uuid = await Editor.Message.request("scene", "create-node", {
            "name": "root",
        })

        let types = ["GameObject", "Transform", "ParticleSystem", "ParticleSystemRenderer"]
        let files = []
        for(let i = 0; i < filesList.length; i++) {
            let jsons = []
            files.push(jsons)
            let file = filesList[i]
            console.log("readFile:" + file)
            let content = fs.readFileSync(path + "/" + file, 'utf8')
            content = content.replaceAll(/%TAG !u!.*?\n/g, "")

            // let sp = content.split(/--- !u!.*?\n/g)
            // !u!199 &
            content = content.replaceAll(/!u!([0-9]+) &/g, " fileID: ")
            // 将fileid转换为string
            content = content.replaceAll("fileID: ", "fileID: id")
            for (let i = 0; i < types.length; i++) {
                content = content.replaceAll("\n" + types[i] + ":\n", "\n  u3dType: " + types[i] + "\n")
            }

            let sp = content.split(/---/g)
            for (let i = 0; i < sp.length; i++) {
                let s = sp[i]
                let json = YAML.parse(s)
                if (json)
                jsons.push(json)
            }
        }

        await Editor.Message.request("scene", "create-component", {
            component: "U3dParticle2Cocos",
            uuid: root_uuid
        })

        const node = await Editor.Message.request('scene', 'query-node', root_uuid);
        const cmp_uuid = node.__comps__[0].value.uuid.value

        await Editor.Message.request("scene", "execute-component-method", {
            uuid: cmp_uuid,
            name: "doConvert",
            args: [files]
        })
    },
    async openPanel (){
        Editor.Panel.open('u3dparticle-cocos.list');
    },
};

/**
 * 启动的时候执行的初始化方法
 * Initialization method performed at startup
 */
exports.load = function() {};

/**
 * 插件被关闭的时候执行的卸载方法
 * Uninstall method performed when the plug-in is closed
 */
exports.unload = function() {};
